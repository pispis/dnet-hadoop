{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "definitions": {
    "ControlledField": {
      "type": "object",
      "properties": {
        "scheme": {
          "type": "string"
        },
        "value": {
          "type": "string"
        }
      },
      "description": "To represent the information described by a scheme and a value in that scheme (i.e. pid)"
    },
    "Provenance": {
      "type": "object",
      "properties": {
        "provenance": {
          "type": "string",
          "description": "The process that produced/provided the information"
        },
        "trust": {
          "type": "string"
        }
      },
      "description": "Indicates the process that produced (or provided) the information, and the trust associated to the information"
    }
  },
  "type": "object",
  "properties": {
    "author": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "fullname": {
            "type": "string"
          },
          "name": {
            "type": "string"
          },
          "pid": {
            "type": "object",
            "properties": {
              "id": {
                "allOf": [
                  {"$ref": "#/definitions/ControlledField"},
                  {"description": "The author's id and scheme. OpenAIRE currently supports 'ORCID'"}
                ]
              },
              "provenance": {
                "allOf": [
                  {"$ref": "#/definitions/Provenance"},
                  {"description": "Provenance of author's pid"}
                ]
              }
            }
          },
          "rank": {
            "type": "integer"
          },
          "surname": {
            "type": "string"
          }
        }
      }
    },
    "bestaccessright": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string",
          "description": "COAR access mode code: http://vocabularies.coar-repositories.org/documentation/access_rights/"
        },
        "label": {
          "type": "string",
          "description": "Label for the access mode"
        },
        "scheme": {
          "type": "string",
          "description": "Scheme of reference for access right code. Always set to COAR access rights vocabulary: http://vocabularies.coar-repositories.org/documentation/access_rights/"
        }
      },
      "description": "The openest access right associated to the manifestations of this research results"
    },
    "codeRepositoryUrl": {
      "type": "string",
      "description": "Only for results with type 'software': the URL to the repository with the source code"
    },
    "contactgroup": {
      "description": "Only for results with type 'software': Information on the group responsible for providing further information regarding the resource",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "contactperson": {
      "description": "Only for results with type 'software': Information on the person responsible for providing further information regarding the resource",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "container": {
      "type": "object",
      "properties": {
        "conferencedate": {
          "type": "string"
        },
        "conferenceplace": {
          "type": "string"
        },
        "edition": {
          "type": "string",
          "description": "Edition of the journal or conference proceeding"
        },
        "ep": {
          "type": "string",
          "description": "End page"
        },
        "iss": {
          "type": "string",
          "description": "Journal issue"
        },
        "issnLinking": {
          "type": "string"
        },
        "issnOnline": {
          "type": "string"
        },
        "issnPrinted": {
          "type": "string"
        },
        "name": {
          "type": "string",
          "description": "Name of the journal or conference"
        },
        "sp": {
          "type": "string",
          "description": "start page"
        },
        "vol": {
          "type": "string"
        }
      },
      "description": "Container has information about the conference or journal where the result has been presented or published"
    },
    "contributor": {
      "type": "array",
      "items": {
        "type": "string",
        "description": "Description of contributor"
      }
    },
    "country": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "code": {
            "type": "string",
            "description": "ISO 3166-1 alpha-2 country code"
          },
          "label": {
            "type": "string"
          },
          "provenance": {
            "allOf": [
              {"$ref": "#/definitions/Provenance"},
              {"description": "Why this result is associated to the country."}
            ]
          }
        }
      }
    },
    "coverage": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "dateofcollection": {
      "type": "string",
      "description": "When OpenAIRE collected the record the last time"
    },
    "description": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "documentationUrl": {
      "description": "Only for results with type 'software': URL to the software documentation",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "embargoenddate": {
      "type": "string",
      "description": "Date when the embargo ends and this result turns Open Access"
    },
    "format": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "geolocation": {
      "description": "Geolocation information",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "box": {
            "type": "string"
          },
          "place": {
            "type": "string"
          },
          "point": {
            "type": "string"
          }
        }
      }
    },
    "id": {
      "type": "string",
      "description": "OpenAIRE Identifier"
    },
    "language": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string",
          "description": "alpha-3/ISO 639-2 code of the language"
        },
        "label": {
          "type": "string",
          "description": "English label"
        }
      }
    },
    "lastupdatetimestamp": {
      "type": "integer",
      "description": "Timestamp of last update of the record in OpenAIRE"
    },
    "maintitle": {
      "type": "string"
    },
    "originalId": {
      "description": "Identifiers of the record at the original sources",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "pid": {
      "description": "Persistent identifiers of the result",
      "type": "array",
      "items": {
        "allOf": [
          {"$ref": "#/definitions/ControlledField"},
          {"description": "scheme: list of available schemes are at https://api.openaire.eu/vocabularies/dnet:pid_types, value: the PID of the result "}
        ]
      }
    },
    "programmingLanguage": {
      "type": "string",
      "description": "Only for results with type 'software': the programming language"
    },
    "publicationdate": {
      "type": "string"
    },
    "publisher": {
      "type": "string"
    },
    "size": {
      "type": "string",
      "description": "Only for results with type 'dataset': the declared size of the dataset"
    },
    "source": {
      "description": "See definition of Dublin Core field dc:source",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "subjects": {
      "description": "Keywords associated to the result",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "provenance": {
            "allOf": [
              {"$ref": "#/definitions/Provenance"},
              {"description": "Why this subject is associated to the result"}
            ]
          },
          "subject": {
            "allOf": [
              {"$ref": "#/definitions/ControlledField"},
              {"description": "OpenAIRE subject classification scheme (https://api.openaire.eu/vocabularies/dnet:subject_classification_typologies) and value. When the scheme is 'keyword', it means that the subject is free-text (i.e. not a term from a controlled vocabulary)."},
            ]
          }
        }
      }
    },
    "subtitle": {
      "type": "string"
    },
    "tool": {
      "description": "Only for results with type 'other': tool useful for the interpretation and/or re-used of the research product",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "type": {
      "type": "string",
      "description": "Type of the result: one of 'publication', 'dataset', 'software', 'other' (see also https://api.openaire.eu/vocabularies/dnet:result_typologies)"
    },
    "version": {
      "type": "string",
      "description": "Version of the result"
    }
  }
}
