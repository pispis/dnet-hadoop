{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "definitions": {
    "AccessRight": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string",
          "description": "COAR access mode code: http://vocabularies.coar-repositories.org/documentation/access_rights/"
        },
        "label": {
          "type": "string",
          "description": "Label for the access mode"
        },
        "scheme": {
          "type": "string",
          "description": "Scheme of reference for access right code. Always set to COAR access rights vocabulary: http://vocabularies.coar-repositories.org/documentation/access_rights/"
        }
      }
    },
    "ControlledField": {
      "type": "object",
      "properties": {
        "scheme": {
          "type": "string",
          "description": "The scheme for the resource"
        },
        "value": {
          "type": "string",
          "description": "the value in the scheme"
        }
      }
    },
    "KeyValue": {
      "type": "object",
      "properties": {
        "key": {
          "type": "string",
          "description": "Description of key"
        },
        "value": {
          "type": "string",
          "description": "Description of value"
        }
      }
    },
    "Provenance": {
      "type": "object",
      "properties": {
        "provenance": {
          "type": "string",
          "description": "The provenance of the information"
        },
        "trust": {
          "type": "string",
          "description": "The trust associated to the information"
        }
      }
    }
  },
  "type": "object",
  "properties": {
    "author": {
      "description": "List of authors of the research results",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "affiliation": {
            "description": "Affiliations of the author",
            "type": "array",
            "items": {
              "type": "string",
              "description": "One of the affiliation of the author"
            }
          },
          "fullname": {
            "type": "string",
            "description": "Fullname of the author"
          },
          "name": {
            "type": "string",
            "description": "First name of the author"
          },
          "pid": {
            "type": "object",
            "properties": {
              "id": {
                "allOf": [
                  {"$ref": "#/definitions/ControlledField"},
                  {"description": "The author's id and scheme. OpenAIRE currently supports 'ORCID'"}
                ]
              },
              "provenance": {
                "allOf": [
                  {"$ref": "#/definitions/Provenance"},
                  {"description": "The provenance of the author's pid"}
                ]
              }
            },
            "description": "Persistent identifier of the author (e.g. ORCID)"
          },
          "rank": {
            "type": "integer",
            "description": "Order in which the author appears in the authors list"
          },
          "surname": {
            "type": "string",
            "description": "Surname of the author"
          }
        },
        "description": "One of the author of the research result"
      }
    },
    "bestaccessright": {
      "allOf": [
        {"$ref": "#/definitions/AccessRight"},
        {"description": "The openest access right associated to the manifestations of this research results"}
      ]
    },
    "codeRepositoryUrl": {
      "type": "string",
      "description": "Only for results with type 'software': the URL to the repository with the source code"
    },
    "collectedfrom": {
      "description": "Information about the sources from which the record has been collected",
      "type": "array",
      "items": {
        "allOf": [
          {"$ref": "#/definitions/KeyValue"},
          {"description": "Key is the OpenAIRE identifier of the data source, value is its name"}
        ]
      }
    },
    "contactgroup": {
      "description": "Only for results with type 'software': Information on the group responsible for providing further information regarding the resource",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "contactperson": {
      "description": "Only for results with type 'software': Information on the person responsible for providing further information regarding the resource",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "container": {
      "type": "object",
      "properties": {
        "conferencedate": {
          "type": "string",
          "description": "Date of the conference"
        },
        "conferenceplace": {
          "type": "string",
          "description": "Place of the conference"
        },
        "edition": {
          "type": "string",
          "description": "Edition of the journal or conference proceeding"
        },
        "ep": {
          "type": "string",
          "description": "End page"
        },
        "iss": {
          "type": "string",
          "description": "Journal issue"
        },
        "issnLinking": {
          "type": "string",
          "description": "Journal linking iisn"
        },
        "issnOnline": {
          "type": "string",
          "description": "Journal online issn"
        },
        "issnPrinted": {
          "type": "string",
          "description": "Journal printed issn"
        },
        "name": {
          "type": "string",
          "description": "Name of the journal or conference"
        },
        "sp": {
          "type": "string",
          "description": "Start page"
        },
        "vol": {
          "type": "string",
          "description": "Volume"
        }
      },
      "description": "Container has information about the conference or journal where the result has been presented or published"
    },
    "context": {
      "description": "Reference to a relevant research infrastructure, initiative or community (RI/RC) among those collaborating with OpenAIRE. Please see https://connect.openaire.eu",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "code": {
            "type": "string",
            "description": "Code identifying the RI/RC"
          },
          "label": {
            "type": "string",
            "description": "Label of the RI/RC"
          },
          "provenance": {
            "description": "Why this result is associated to the RI/RC.",
            "type": "array",
            "items": {
              "allOf": [
                {"$ref": "#/definitions/Provenance"}

              ]
            }
          }
        }
      }
    },
    "contributor": {
      "description": "Contributors of this result",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "country": {
      "description": "Country associated to this result",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "code": {
            "type": "string",
            "description": "ISO 3166-1 alpha-2 country code"
          },
          "label": {
            "type": "string",
            "description": "English label of the country"
          },
          "provenance": {
            "allOf": [
              {"$ref": "#/definitions/Provenance"},
              {"description": "Why this result is associated to the country."}
            ]
          }
        }
      }
    },
    "coverage": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "dateofcollection": {
      "type": "string",
      "description": "When OpenAIRE collected the record the last time"
    },
    "description": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "documentationUrl": {
      "description": "Only for results with type 'software': URL to the software documentation",
      "type": "array",
      "items": {
        "type": "string"

      }
    },
    "embargoenddate": {
      "type": "string",
      "description": "Date when the embargo ends and this result turns Open Access"
    },
    "externalReference": {
      "description": "Links to external resources like entries from thematic databases (e.g. Protein Data Bank)",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          },
          "provenance": {
            "allOf": [
              {"$ref": "#/definitions/Provenance"},
              {"description": "Why this result is linked to the external resource"}
            ]
          },
          "typology": {
            "type": "string"
          },
          "value": {
            "type": "string"
          }
        }
      }
    },
    "format": {

      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "geolocation": {
      "description": "Geolocation information",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "box": {
            "type": "string"
          },
          "place": {
            "type": "string"
          },
          "point": {
            "type": "string"
          }
        }
      }
    },
    "id": {
      "type": "string",
      "description": "OpenAIRE identifier"
    },
    "instance": {
      "description": "Manifestations (i.e. different versions) of the result. For example: the pre-print and the published versions are two manifestations of the same research result",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "accessright": {
            "allOf": [
              {"$ref": "#/definitions/AccessRight"},
              {"description": "Access right of this instance"}
            ]
          },
          "collectedfrom": {
            "allOf": [
              {"$ref": "#/definitions/KeyValue"},
              {"description": "Information about the source from which the instance has been collected. Key is the OpenAIRE identifier of the data source, value is its name"}
            ]
          },
          "hostedby": {
            "allOf": [
              {"$ref": "#/definitions/KeyValue"},
              {"description": "Information about the source from which the instance can be viewed or downloaded. Key is the OpenAIRE identifier of the data source, value is its name"}
            ]
          },
          "license": {
            "type": "string",
            "description": "License applied to the instance"
          },
          "publicationdate": {
            "type": "string",
            "description": "Publication date of the instance"
          },
          "refereed": {
            "type": "string",
            "description": "Was the instance subject to peer-review? Possible values are 'Unknown', 'nonPeerReviewed', 'peerReviewed' (see also https://api.openaire.eu/vocabularies/dnet:review_levels)"
          },
          "type": {
            "type": "string",
            "description": "Type of the instance. Possible values are listed at https://api.openaire.eu/vocabularies/dnet:publication_resource"
          },
          "url": {
            "description":"Location where the instance is accessible",
            "type": "array",
            "items": {
              "type": "string"
            }
          }
        }
      }
    },
    "language": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string",
          "description": "alpha-3/ISO 639-2 code of the language"
        },
        "label": {
          "type": "string",
          "description": "English label"
        }
      }
    },
    "lastupdatetimestamp": {
      "type": "integer",
      "description": "Timestamp of last update of the record in OpenAIRE"
    },
    "maintitle": {
      "type": "string",
      "description": "Title"
    },
    "originalId": {
      "description": "Identifiers of the record at the original sources",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "pid": {
      "description": "Persistent identifiers of the result",
      "type": "array",
      "items": {
        "allOf": [
          {"$ref": "#/definitions/ControlledField"},
          {"description": "scheme: list of available schemes are at https://api.openaire.eu/vocabularies/dnet:pid_types, value: the PID of the result "}
        ]
      }
    },
    "programmingLanguage": {
      "type": "string",
      "description": "Only for results with type 'software': the programming language"
    },
    "projects": {
      "description": "List of projects (i.e. grants) that (co-)funded the production ofn the research results",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "acronym": {
            "type": "string",
            "description": "Project acronym"
          },
          "code": {
            "type": "string",
            "description": "Grant code"
          },
          "funder": {
            "type": "object",
            "properties": {
              "fundingStream": {
                "type": "string",
                "description": "Stream of funding (e.g. for European Commission can be H2020 or FP7)"
              },
              "jurisdiction": {
                "type": "string",
                "description": "Geographical jurisdiction (e.g. for European Commission is EU, for Croatian Science Foundation is HR)"
              },
              "name": {
                "type": "string",
                "description": "Name of the funder"
              },
              "shortName": {
                "type": "string",
                "description": "Short name or acronym of the funder"
              }
            },
            "description": "Information about the funder funding the project"
          },
          "id": {
            "type": "string",
            "description": "OpenAIRE identifier of the project"
          },
          "provenance": {
            "allOf": [
              {"$ref": "#/definitions/Provenance"},
              {"description": "Why this project is associated to the result"}
            ]
          },
          "title": {
            "type": "string",
            "description": "Title of the project"
          }
        }
      }
    },
    "publicationdate": {
      "type": "string",
      "description": "Date of publication"
    },
    "publisher": {
      "type": "string",
      "description": "Publisher"
    },
    "size": {
      "type": "string",
      "description": "Only for results with type 'dataset': the declared size of the dataset"
    },
    "source": {
      "description": "See definition of Dublin Core field dc:source",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "subjects": {
      "description": "Keywords associated to the result",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "provenance": {
            "allOf": [
              {"$ref": "#/definitions/Provenance"},
              {"description": "Why this subject is associated to the result"}
            ]
          },
          "subject": {
            "allOf": [
              {"$ref": "#/definitions/ControlledField"},
              {"description": "OpenAIRE subject classification scheme (https://api.openaire.eu/vocabularies/dnet:subject_classification_typologies) and value. When the scheme is 'keyword', it means that the subject is free-text (i.e. not a term from a controlled vocabulary). "}
            ]
          }
        }
      }
    },
    "subtitle": {
      "type": "string",
      "description": "Sub-title of the result"
    },
    "tool": {
      "description": "Only for results with type 'other': tool useful for the interpretation and/or re-used of the research product",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "type": {
      "type": "string",
      "description": "Type of the result: one of 'publication', 'dataset', 'software', 'other' (see also https://api.openaire.eu/vocabularies/dnet:result_typologies)"
    },
    "version": {
      "type": "string",
      "description": "Version of the result"
    }
  }
}