
package eu.dnetlib.dhp.oa.graph.dump.complete;

import java.io.Serializable;

public class Constants implements Serializable {

	public static final String IS_HOSTED_BY = "isHostedBy";
	public static final String HOSTS = "hosts";

	public static final String IS_FUNDED_BY = "isFundedBy";
	public static final String FUNDS = "funds";

	public static final String FUNDINGS = "fundings";

	public static final String RESULT_ENTITY = "result";
	public static final String DATASOURCE_ENTITY = "datasource";
	public static final String CONTEXT_ENTITY = "context";
	public static final String ORGANIZATION_ENTITY = "organization";
	public static final String PROJECT_ENTITY = "project";

	public static final String CONTEXT_ID = "00";
	public static final String CONTEXT_NS_PREFIX = "context_____";
	public static final String UNKNOWN = "UNKNOWN";

	// public static final String FUNDER_DS = "entityregistry::projects";
}
